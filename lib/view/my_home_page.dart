import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

List<Widget> headerWidget = [
  Column(
    children: [
      Image.asset(
        "assets/images/film-roll.png",
        scale: 8,
      ),
      Text(
        "Movies",
        style: TextStyle(color: Colors.black, fontSize: 12),
      ),
    ],
  ),
];

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 6, 1, 19),
        leading: const Column(
          children: [
            SizedBox(
              width: 2,
            ),
            Text(
              "It All Starts Here",
              style: TextStyle(
                  fontSize: 25,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Kolkata >",
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
        leadingWidth: 212.0,
        actions: const [
          Icon(
            Icons.search,
            color: Colors.white,
            size: 26,
          ),
          SizedBox(
            width: 20,
          ),
          Icon(
            Icons.notifications_outlined,
            color: Colors.white,
            size: 26,
          ),
          SizedBox(
            width: 20,
          ),
          Icon(
            Icons.qr_code_scanner_sharp,
            color: Colors.white,
            size: 26,
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
      body: Column(
        children: [],
      ),
    );
  }
}
